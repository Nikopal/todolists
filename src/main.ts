import Vue from 'vue'
import App from '@/App.vue'
import store from '@/store'
import router from '@/router'
import 'vuetify/dist/vuetify.min.css'
import Vuetify from "vuetify";


Vue.config.productionTip = false
Vue.use(Vuetify);

const vuetify = new Vuetify({
  lang: {
      current: 'ru',
  },
  icons: {
      iconfont: "mdi"
  },
});
new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
