import Vue from 'vue';
import VueRouter from 'vue-router';
import toDo from '@/pages/todo/toDo.vue';
Vue.use(VueRouter);
var routes = [
    {
        path: '/',
        name: 'todo',
        component: toDo
    },
];
var router = new VueRouter({
    routes: routes
});
export default router;
//# sourceMappingURL=index.js.map