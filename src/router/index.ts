import Vue from 'vue'
import VueRouter from 'vue-router'
import toDo from '@/pages/todo/toDo.vue'
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'todo',
        component: toDo
    },

]

const router = new VueRouter({
    routes
})

export default router
