import {todo} from './types/types'

export default {
    strict: true,
    namespaced: true,
    state: () => ({
        todo: [],
    }),
    getters: {
        todo: (state: { todo: Array<Object> }) => state.todo,
        todoToday: (state: { todo: any[]; }) => state.todo.filter(item => item.seldw === 0),
        todoTomorrow: (state:{ todo: any[]; }) => state.todo.filter(item => item.seldw == 1),
    },
    mutations: {
        setTodo(state: any, payload: todo) {
            state.todo.unshift({
                title: payload.title,
                discr: payload.discr,
                seldw: payload.seldw,
                flag: false,
            })
        },
        delTodo(state: any, payload: number) {
            state.todo.splice(payload, 1);
        },
        successTodo(state:any, payload:number){
            state.todo[payload].flag = true;
        },
        successTodoAll(state:any){
            for(let i in state.todo){
                if(state.todo[i].seldw === 0){
                    state.todo[i].flag = true;
                }
            }
        }
    },
    actions: {
        setTodo: ({commit, dispatch}: any, payload: todo) => {
            return new Promise((resolve) => {
                if (payload.title !== '') {
                    console.log(payload);
                    commit('setTodo', payload)
                    dispatch('alert/act_snackbar', {
                        flag: true,
                        color: 'success',
                        text: 'Задача добавлена'
                    }, {root: true})
                    resolve(true)
                } else {
                    // @ts-ignore
                    resolve(false)
                    dispatch('alert/act_snackbar', {
                        flag: true,
                        color: 'warning',
                        text: 'Введите название задания'
                    }, {root: true})
                }
            })

        },
        setSuccessTodo: ({commit}:any, payload:number) => {
            commit('successTodo', payload)
        },
        setSuccessTodoAll: ({commit}:any) => {
            commit('successTodoAll')
        },
        delTodo: ({commit}: any, payload: number) => {
            commit('delTodo', payload);
        }
    }
}