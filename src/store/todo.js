export default {
    strict: true,
    namespaced: true,
    state: function () { return ({
        todo: [],
    }); },
    getters: {
        todo: function (state) { return state.todo; },
        todoToday: function (state) { return state.todo.filter(function (item) { return item.seldw === 0; }); },
        todoTomorrow: function (state) { return state.todo.filter(function (item) { return item.seldw == 1; }); },
    },
    mutations: {
        setTodo: function (state, payload) {
            state.todo.unshift({
                title: payload.title,
                discr: payload.discr,
                seldw: payload.seldw,
                flag: false,
            });
        },
        delTodo: function (state, payload) {
            state.todo.splice(payload, 1);
        },
        successTodo: function (state, payload) {
            state.todo[payload].flag = true;
        },
        successTodoAll: function (state) {
            for (var i in state.todo) {
                if (state.todo[i].seldw === 0) {
                    state.todo[i].flag = true;
                }
            }
        }
    },
    actions: {
        setTodo: function (_a, payload) {
            var commit = _a.commit, dispatch = _a.dispatch;
            return new Promise(function (resolve) {
                if (payload.title !== '') {
                    console.log(payload);
                    commit('setTodo', payload);
                    dispatch('alert/act_snackbar', {
                        flag: true,
                        color: 'success',
                        text: 'Задача добавлена'
                    }, { root: true });
                    resolve(true);
                }
                else {
                    // @ts-ignore
                    resolve(false);
                    dispatch('alert/act_snackbar', {
                        flag: true,
                        color: 'warning',
                        text: 'Введите название задания'
                    }, { root: true });
                }
            });
        },
        setSuccessTodo: function (_a, payload) {
            var commit = _a.commit;
            commit('successTodo', payload);
        },
        setSuccessTodoAll: function (_a) {
            var commit = _a.commit;
            commit('successTodoAll');
        },
        delTodo: function (_a, payload) {
            var commit = _a.commit;
            commit('delTodo', payload);
        }
    }
};
//# sourceMappingURL=todo.js.map