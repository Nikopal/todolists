import Vue from 'vue';
import Vuex from 'vuex';
import todo from "@/store/todo";
import alert from "@/store/alert";
Vue.use(Vuex);
export default new Vuex.Store({
    modules: {
        todo: todo,
        alert: alert
    }
});
//# sourceMappingURL=index.js.map