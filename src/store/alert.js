export default {
    strict: true,
    namespaced: true,
    state: function () { return ({
        snackbar: {
            flag: false,
            color: '',
            text: '',
        },
    }); },
    mutations: {
        set_snackbar: function (state, payload) {
            state.snackbar = {
                text: payload.text,
                flag: payload.flag,
                color: payload.color,
            };
        },
    },
    actions: {
        act_snackbar: function (_a, payload) {
            var commit = _a.commit;
            commit('set_snackbar', payload);
        },
    },
    getters: {
        get_snackbar: function (state) { return state.snackbar; },
    },
};
//# sourceMappingURL=alert.js.map