
export default {
    strict: true,
    namespaced: true,
    state:()=>({
        snackbar:{
            flag:false,
            color:'',
            text:'',
        },
    }),
    mutations:{
        set_snackbar:(state:any, payload:any) =>{
            state.snackbar = {
                text: payload.text,
                flag: payload.flag,
                color: payload.color,
            }
        },
    },
    actions:{
        act_snackbar({commit}: any, payload: any){
            commit('set_snackbar', payload)
        },
    },
    getters:{
        get_snackbar: (state: { snackbar: Object; }) => state.snackbar,
    },
}